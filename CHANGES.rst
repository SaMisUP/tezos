Development Changelog
'''''''''''''''''''''

**NB:** The changelog for releases can be found at: https://tezos.gitlab.io/CHANGES.html


This file lists the changes added to each version of octez-node,
octez-client, and the other Octez executables. The changes to the economic
protocol are documented in the ``docs/protocols/`` directory; in
particular in ``docs/protocols/alpha.rst``.

When you make a commit on master, you can add an item in one of the
following subsections (node, client, …) to document your commit or the
set of related commits. This will ensure that this change is not
forgotten in the final changelog, which can be found in ``docs/CHANGES.rst``.
By having your commits update this file you also make it easy to find the
commits which are related to your changes using ``git log -p -- CHANGES.rst``.
Relevant items are moved to ``docs/CHANGES.rst`` after each release.

Only describe changes which affect users (bug fixes and new features),
or which will affect users in the future (deprecated features),
not refactorings or tests. Changes to the documentation do not need to
be documented here either.

Node
----

- Fixed a bug that caused snapshot import to ignore the data directory
  of the configuration file when the configuration file was specified
  from the command-line using ``--config-file``. Note that ``--data-dir``
  can still be used to override the data directory location from the
  configuration file, whether it is specified from the command-line or not.

- Fixed a bug that caused the ``snapshot import`` command to fail when
  used on data directories configured with an explicit number of
  additional cycles.

- Fixed an issue that could left a temporary directory if a snapshot
  export was cancelled. Additionally, a cleanup now ensures the
  absence of leftovers temporary directories when exporting a
  snapshot.

- Fixed an issue that could left a lock file if a snapshot import was
  cancelled.

- **Breaking change**: the default ``?version`` of the ``pending_operations``
  RPC is now 1 instead of 0. Version 1 is more consistent as
  ``branch_delayed``/``branch_refused``/``outdated``/``refused`` operations are
  encoded like ``applied`` operations: the ``"hash"`` field is included in the
  object instead of being separate in an array. The same change applies to
  ``unprocessed`` operations, except that those do not contain the ``error``
  field. More details can be found by calling the
  ``describe/chains/main/mempool/pending_operations`` RPC. You can get back the
  previous encoding with ``?version=0`` but note that version 0 is now
  deprecated and may be removed starting from the next major release of Octez.
  (MR :gl:`!6783`)

- The ``pending_operations`` RPC can now be run in ``binary`` format when using
  version ``1``. (MR :gl:`!6783`)

- Removed the ``node_`` prefix in identifiers of the
  ``config_validation`` and ``config_file`` events and errors.

- Introduced a ``--json`` command line argument to the ``snapshot
  info`` allowing to print snapshot information as JSON.

- Removed the ``octez-validator`` executable, which was already part
  of ``octez-node`` and that was already used internally (and that was
  not usable on its own).

- **Breaking change**: bumped the node's storage version to
  ``3.0``. This new version changes the store's representation
  required by the new protocol's semantics. Upgrading to this new
  version is automatic and irreversible.

- **Breaking change**: bumped the snapshot version to ``5``. This
  version changes internal snapshot file representation to include
  more information required by the new protocol's semantics. Snapshots
  of version ``4`` exported with previous versions of Octez can still
  be imported. Snapshots of version ``5`` are not backward compatible.

- Removed the compatibility with storage snapshots of version ``2``
  and ``3``. These snapshot versions from Octez 12 cannot be imported
  anymore.

- Added optional query parameter ``validation_pass`` to RPCs ``GET
  /chains/main/mempool/pending_operations`` and ``GET
  /chains/<chain_id>/mempool/monitor_operation``. This new parameter causes the
  RPC to only return operations for the given validation pass (``0`` for
  consensus operations, ``1`` for voting operations, ``2`` for anonymous
  operations, ``3`` for manager operations). If ``validation_pass`` is
  unspecified, operations for all validation passes are returned, making this
  extension backward-compatible. (MR :gl:`!6724`)

Client
------

- Added command to get contract's balance of ticket with specified ticketer, content type, and content. Can be used for both implicit and originated contracts.
  ``octez-client get ticket balance for <contract> with ticketer '<ticketer>' and type <type> and content <content>``. (MR :gl:`!6491`)

- Added command to get the complete list of tickets owned by a given contract by scanning the contract's storage. Can only be used for originated contracts.
  ``octez-client get all ticket balances for <contract>``. (MR :gl:`!6804`)

Baker
-----

- **Breaking change**: modified the baker's persistent state. Once the
  protocol "M" activates, the new baker will automatically overwrite
  the existing persistent state to the new format. This implies that
  previous bakers will fail to load this new state from disk unless
  the user directly removes the file
  ``<client-dir>/<chain_id>_baker_state``. On mainnet, this will have
  no effect as when the new protocol activates, previous bakers will
  be permanently idle.

- Fixed an issue where the baker would keep files opened longer than
  necessary causing unexpected out of space errors making the baker
  crash.

Accuser
-------

Signer
------

Proxy Server
------------

- The proxy server can now serve endpoints about blocks of all known economic
  protocols instead of only one chosen at boot time.

Protocol Compiler And Environment
---------------------------------

Codec
-----

- Added the ``dump encoding <id>`` command to dump the description of a single
  registered encoding.

Docker Images
-------------

Rollups
-------

Miscellaneous
-------------

- Versioning of signature module for protocol specific support and future
  extensibility.
